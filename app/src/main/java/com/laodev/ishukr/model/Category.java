package com.laodev.ishukr.model;

import java.io.Serializable;

public class Category implements Serializable {

    public String category_id = "";
    public String parent_id = "";
    public String name = "";
    public String seo_url = "";
    public String image = "";
    public String original_image = "";

}
