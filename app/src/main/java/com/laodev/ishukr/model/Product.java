package com.laodev.ishukr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Product implements Serializable {
    public int id = 0;
    public int product_id = 0;
    public String name = "";
    public String manufacturer = "";
    public String sku = "";
    public String model = "";
    public String image = "";
    public String original_image = "";
    public String price_excluding_tax_formated = "";
    public String price_formated = "";
    public String description = "";
    public String delivery_returns_info = "";
    public int quantity = 0;
    public double price_excluding_tax = 0.0;
    public double price = 0.0;

    public List<String> images = new ArrayList<>();
    public List<String> original_images = new ArrayList<>();
//    public List<String> options = new ArrayList<>();
}
