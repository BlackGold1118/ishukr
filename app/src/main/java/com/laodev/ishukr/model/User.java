package com.laodev.ishukr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {
    public String customer_id = "";
    public String customer_group_id = "";
    public String store_id = "";
    public String language_id = "";
    public String firstname = "";
    public String lastname = "";
    public String email = "";
    public String telephone = "";
    public String fax = "";
    public String wishlist = "";
    public String newsletter = "";
    public String address_id = "";
    public String ip = "";
    public String status = "";
    public String safe = "";
    public String code = "";
    public String device_token = "";
    public String date_added = "";
//    public String custom_fields = "";
    public boolean account_custom_field = false;
    public int cart_count_products = 0;

}
