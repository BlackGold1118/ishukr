package com.laodev.ishukr.model;

import java.io.Serializable;

public class PaymentAccount implements Serializable {
    public String card_hold_name = "";
    public String expired = "";
    public String cvv = "";
    public String card_number = "";
    public boolean isDefault = false;
}
