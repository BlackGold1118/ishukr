package com.laodev.ishukr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CartModel implements Serializable {

    public String weight = "";
    public String coupon_status = "";
    public String coupon = "";
    public String voucher_status = "";
    public String voucher = "";
    public boolean reward_status = false;
    public String reward = "";
    public String total = "";
    public int total_product_count = 0;
    public int has_shipping = 0;
    public int has_download = 0;
    public int has_recurring_products = 0;
    public double total_raw = 0;

}
