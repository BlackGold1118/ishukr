package com.laodev.ishukr.model;

import java.io.Serializable;

public class Banner implements Serializable {
    public String title = "";
    public String link = "";
    public String image = "";
}
