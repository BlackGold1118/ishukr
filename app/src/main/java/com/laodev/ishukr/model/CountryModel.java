package com.laodev.ishukr.model;

import java.io.Serializable;

public class CountryModel implements Serializable {

    public String address_format = "";
    public String country_id = "";
    public String iso_code_2 = "";
    public String iso_code_3 = "";
    public String name = "";
    public String postcode_required = "";
    public String status = "";

}
