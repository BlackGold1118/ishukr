package com.laodev.ishukr.model;

import java.io.Serializable;

public class Media implements Serializable {

    public String article_id = "";
    public String heading_title = "";
    public String additional_description = "";
    public String date = "";
    public String description = "";

}
