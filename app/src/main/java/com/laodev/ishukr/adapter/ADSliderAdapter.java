package com.laodev.ishukr.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.ishukr.R;
import com.laodev.ishukr.model.Banner;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ADSliderAdapter extends SliderViewAdapter<ADSliderAdapter.SliderAdapterVH> {

    private Context context;
    private List<Banner> mModels;


    public ADSliderAdapter(Context context, List<Banner> models) {
        this.context = context;
        mModels = models;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ad_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        Banner model = mModels.get(position);
        Picasso.get().load(model.image).fit()
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(viewHolder.imageViewBackground, null);
        viewHolder.itemView.setOnClickListener(view -> {
            if (model.link.isEmpty()) {
                return;
            }
            String url = model.link;
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mModels.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.img_ad_slider);
            this.itemView = itemView;
        }
    }

}
