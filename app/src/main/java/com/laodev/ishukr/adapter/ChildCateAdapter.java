package com.laodev.ishukr.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.fragment.ProductDetailFragment;
import com.laodev.ishukr.fragment.SubCateFragment;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.model.Product;
import com.laodev.ishukr.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChildCateAdapter extends BaseAdapter {

    private MainActivity mainActivity;
    private List<Product> products;


    public ChildCateAdapter(MainActivity mainActivity, List<Product> products) {
        this.mainActivity = mainActivity;
        this.products = products;
    }

    @Override
    public int getCount() {
        return (products.size() + 1) / 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mainActivity).inflate(R.layout.adapter_child_category_grid, null);

        Product product1 = products.get(position * 2);

        CardView crd_01 = convertView.findViewById(R.id.crd_01);
        ImageView img_cate_01 = convertView.findViewById(R.id.img_cate_01);
        TextView lbl_cost_01 = convertView.findViewById(R.id.lbl_cost_01);
        TextView lbl_desc_01 = convertView.findViewById(R.id.lbl_desc_01);
        Picasso.get()
                .load(product1.image)
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(img_cate_01);
        lbl_cost_01.setText(product1.price_formated);
        lbl_desc_01.setText(product1.name);
        crd_01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager()
                        .beginTransaction();
                Fragment productDetailFrg = new ProductDetailFragment(mainActivity, product1);
                fragmentTransaction.replace(R.id.frg_body, productDetailFrg);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        CardView crd_02 = convertView.findViewById(R.id.crd_02);
        if (position * 2 + 1 > products.size() - 1) {
            crd_02.setVisibility(View.INVISIBLE);
        } else {
            Product product2 = products.get(position * 2 + 1);
            ImageView img_cate_02 = convertView.findViewById(R.id.img_cate_02);
            TextView lbl_cost_02 = convertView.findViewById(R.id.lbl_cost_02);
            TextView lbl_desc_02 = convertView.findViewById(R.id.lbl_desc_02);
            Picasso.get()
                    .load(product2.image)
                    .placeholder(R.drawable.ic_image_blue)
                    .error(R.drawable.ic_image_blue)
                    .into(img_cate_02);
            lbl_cost_02.setText(product2.price_formated);
            lbl_desc_02.setText(product2.name);
            crd_02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager()
                            .beginTransaction();
                    Fragment productDetailFrg = new ProductDetailFragment(mainActivity, product2);
                    fragmentTransaction.replace(R.id.frg_body, productDetailFrg);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }

        return convertView;
    }

}
