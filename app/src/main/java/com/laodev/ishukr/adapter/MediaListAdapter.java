package com.laodev.ishukr.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.activity.MediaDetailsActivity;
import com.laodev.ishukr.model.Media;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.AppManager;

import java.util.List;

public class MediaListAdapter extends BaseAdapter {

    private MainActivity mainActivity;
    private List<Media> mediaList;


    public MediaListAdapter(MainActivity mainActivity, List<Media> mediaList) {
        this.mainActivity = mainActivity;
        this.mediaList = mediaList;
    }

    @Override
    public int getCount() {
        return mediaList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Media media = mediaList.get(position);

        convertView = LayoutInflater.from(mainActivity).inflate(R.layout.adapter_media_list, null);
        TextView lbl_head = convertView.findViewById(R.id.lbl_head);
        TextView lbl_desc = convertView.findViewById(R.id.lbl_desc);

        lbl_head.setText(media.heading_title);
        lbl_desc.setText(media.description);

        convertView.setOnClickListener(v -> {
            AppConstants.gSelMedia = media;
            AppManager.showOtherActivity(mainActivity, MediaDetailsActivity.class, 0);
        });

        return convertView;
    }
}
