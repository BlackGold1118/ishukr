package com.laodev.ishukr.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.fragment.ProductListFragment;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.view.AccountView;

import java.util.List;

public class SubCateAdapter extends BaseAdapter {

    private MainActivity mainActivity;
    private List<Category> categories;


    public SubCateAdapter(MainActivity mainActivity, List<Category> categories) {
        this.mainActivity = mainActivity;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Category category = categories.get(position);

        AccountView accountView = new AccountView(mainActivity, category);
        accountView.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager()
                    .beginTransaction();
            Fragment subCateFragment = new ProductListFragment(mainActivity, category);
//                subCateFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frg_body, subCateFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        return accountView;
    }
}
