package com.laodev.ishukr.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;

import com.google.gson.Gson;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.AddPaymentActivity;
import com.laodev.ishukr.model.PaymentAccount;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.AppManager;
import com.laodev.ishukr.util.SharePreferenceUtil;
import com.laodev.ishukr.util.StringUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaymentAdapter extends BaseAdapter {

    private Context context;
    private List<PaymentAccount> paymentAccounts;
    private SparseArray<Pattern> mCCPatterns = null;

    private PaymentAdapterListener paymentAdapterListener;


    public PaymentAdapter(Context context, List<PaymentAccount> paymentAccounts) {
        this.context = context;
        this.paymentAccounts = paymentAccounts;
        if (mCCPatterns == null) {
            mCCPatterns = new SparseArray<>();
            mCCPatterns.put(R.drawable.visa, Pattern.compile(
                    "^4[0-9]{2,12}(?:[0-9]{3})?$"));
            mCCPatterns.put(R.drawable.mastercard, Pattern.compile(
                    "^5[1-5][0-9]{1,14}$"));
            mCCPatterns.put(R.drawable.amex, Pattern.compile(
                    "^3[47][0-9]{1,13}$"));
        }
    }

    @Override
    public int getCount() {
        return paymentAccounts.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PaymentAccount account = paymentAccounts.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.adapter_payment, null);

        ImageView img_check = convertView.findViewById(R.id.img_check);
        if (account.isDefault) {
            img_check.setImageResource(R.drawable.ic_check_on);
        } else {
            img_check.setImageResource(R.drawable.ic_check_off);
        }
        ImageView img_card = convertView.findViewById(R.id.img_card);
        int mDrawableResId = 0;
        for (int i = 0; i < mCCPatterns.size(); i++) {
            int key = mCCPatterns.keyAt(i);
            // get the object by the key.
            Pattern p = mCCPatterns.get(key);
            Matcher m = p.matcher(account.card_number);
            if (m.find()) {
                mDrawableResId = key;
                break;
            }
        }
        img_card.setImageResource(mDrawableResId);
        TextView lbl_cvv = convertView.findViewById(R.id.lbl_cvv);
        lbl_cvv.setText(account.cvv);
        TextView lbl_card = convertView.findViewById(R.id.lbl_card);
        lbl_card.setText(StringUtil.getSecurityCardNumber(account.card_number));
        TextView lbl_expire = convertView.findViewById(R.id.lbl_expire);
        lbl_expire.setText("EXP. " + account.expired);
        ImageView img_menu = convertView.findViewById(R.id.img_menu);

        img_menu.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(context, img_menu);
            popupMenu.getMenuInflater().inflate(R.menu.menu_more, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.action_delete:
                        new AlertDialog.Builder(context)
                                .setTitle("Delete Payment Account")
                                .setMessage("Are you sure you want to delete this account?")
                                .setPositiveButton(android.R.string.yes, (dialog, which) -> paymentAdapterListener.onDeletePayAccount(account))
                                .setNegativeButton(android.R.string.no, null)
                                .setIcon(R.mipmap.ic_launcher)
                                .show();
                        break;
                    case R.id.action_edit:
                        AppConstants.gPayAccountIndex = position;
                        AppManager.showOtherActivity(context, AddPaymentActivity.class, 0);
                        break;
                }
                return false;
            });
            popupMenu.show();
        });

        convertView.setOnClickListener(v -> {
            if (account.isDefault) {
                Toast.makeText(context, R.string.toast_alrady_default, Toast.LENGTH_SHORT).show();
                return;
            }
            for (PaymentAccount paymentAccount: paymentAccounts) {
                paymentAccount.isDefault = false;
            }
            account.isDefault = true;
            SharePreferenceUtil.setPaymentAccount(new Gson().toJson(paymentAccounts));

            notifyDataSetChanged();
        });

        return convertView;
    }

    public void setPaymentAdapterListener(PaymentAdapterListener paymentAdapterListener) {
        this.paymentAdapterListener = paymentAdapterListener;
    }

    public interface PaymentAdapterListener {
        void onDeletePayAccount(PaymentAccount account);
    }

}
