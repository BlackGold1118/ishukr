package com.laodev.ishukr.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.fragment.SubCateFragment;
import com.laodev.ishukr.model.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CateGridAdapter extends BaseAdapter {

    private MainActivity mainActivity;
    private List<Category> categories;


    public CateGridAdapter (MainActivity mainActivity, List<Category> categories) {
        this.mainActivity = mainActivity;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return (categories.size() + 1) / 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mainActivity).inflate(R.layout.adapter_category_grid, null);

        Category category1 = categories.get(position * 2);

        CardView crd_01 = convertView.findViewById(R.id.crd_01);
        ImageView img_cate_01 = convertView.findViewById(R.id.img_cate_01);
        TextView lbl_name_01 = convertView.findViewById(R.id.lbl_name_01);
        Picasso.get()
                .load(category1.image)
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(img_cate_01);
        lbl_name_01.setText(category1.name);
        crd_01.setOnClickListener(v -> {
            FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager()
                    .beginTransaction();
            Fragment subCateFragment = new SubCateFragment(mainActivity, category1);
//                subCateFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frg_body, subCateFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        CardView crd_02 = convertView.findViewById(R.id.crd_02);
        if (position * 2 + 1 > categories.size() - 1) {
            crd_02.setVisibility(View.INVISIBLE);
        } else {
            Category category2 = categories.get(position * 2 + 1);

            ImageView img_cate_02 = convertView.findViewById(R.id.img_cate_02);
            TextView lbl_name_02 = convertView.findViewById(R.id.lbl_name_02);
            Picasso.get()
                    .load(category2.image)
                    .placeholder(R.drawable.ic_image_blue)
                    .error(R.drawable.ic_image_blue)
                    .into(img_cate_02);
            lbl_name_02.setText(category2.name);
            crd_02.setOnClickListener(v -> {
                FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager()
                        .beginTransaction();
                Fragment subCateFragment = new SubCateFragment(mainActivity, category2);
//                subCateFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.frg_body, subCateFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            });
        }

        return convertView;
    }

}
