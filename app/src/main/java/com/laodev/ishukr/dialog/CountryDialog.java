package com.laodev.ishukr.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;

import com.laodev.ishukr.R;
import com.laodev.ishukr.model.CountryModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CountryDialog extends Dialog {

    private List<CountryModel> countryModels;
    private List<CountryModel> showModels = new ArrayList<>();
    private List<String> countryNames = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;

    private CountryDialogListener countryDialogListener;


    public CountryDialog(@NonNull Context context, List<CountryModel> countryModels) {
        super(context);
        setContentView(R.layout.dialog_country);

        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        setTitle(null);
        setCanceledOnTouchOutside(false);

        this.countryModels = countryModels;
        showModels.addAll(countryModels);

        initView();
    }

    private void initView() {
        ListView lst_country = findViewById(R.id.lst_country);
        for (CountryModel country: showModels) {
            countryNames.add(country.name);
        }
        countryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, countryNames);
        lst_country.setAdapter(countryAdapter);

        TextView lbl_no_country = findViewById(R.id.lbl_no_country);
        lbl_no_country.setVisibility(View.GONE);

        SearchView scv_country = findViewById(R.id.scv_country);
        scv_country.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                showModels.clear();
                if (newText.isEmpty()) {
                    showModels.addAll(countryModels);
                } else {
                    for (CountryModel countryModel: countryModels) {
                        if (countryModel.name.toLowerCase().contains(newText.toLowerCase())) {
                            showModels.add(countryModel);
                        }
                    }
                }
                countryNames.clear();
                for (CountryModel country: showModels) {
                    countryNames.add(country.name);
                }
                countryAdapter.notifyDataSetChanged();

                if (countryNames.size() == 0) {
                    lbl_no_country.setVisibility(View.VISIBLE);
                    lst_country.setVisibility(View.GONE);
                } else {
                    lbl_no_country.setVisibility(View.GONE);
                    lst_country.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });

        lst_country.setOnItemClickListener((parent, view, position, id) -> {
            CountryModel countryModel = showModels.get(position);
            countryDialogListener.onClick(countryModel);
            dismiss();
        });
    }

    public void setCountryDialogListener(CountryDialogListener countryDialogListener) {
        this.countryDialogListener = countryDialogListener;
    }

    public interface CountryDialogListener {
        void onClick(CountryModel countryModel);
    }

}
