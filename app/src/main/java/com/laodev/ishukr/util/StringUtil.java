package com.laodev.ishukr.util;

public class StringUtil {

    public static String getFirstUpperCharacter(String str) {
        return String.valueOf(str.toUpperCase().charAt(0));
    }

    public static String getSecurityCardNumber(String card) {
        String card_last = card.substring(12);
        return "**** **** **** " + card_last;
    }

}
