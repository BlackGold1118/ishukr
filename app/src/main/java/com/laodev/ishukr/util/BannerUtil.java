package com.laodev.ishukr.util;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.laodev.ishukr.R;

public class BannerUtil {

    public static void onShowSuccessAlertEvent(View view, String alert, int milliseconds) {
        Snackbar.make(view, alert, milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorGreen))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }

    public static void onShowSuccessAlertEvent(View view, int id, int milliseconds) {
        Snackbar.make(view, view.getContext().getString(id), milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorGreen))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }

    public static void onShowErrorAlertEvent(View view, String alert, int milliseconds) {
        Snackbar.make(view, alert, milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorRed))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }

    public static void onShowErrorAlertEvent(View view, int id, int milliseconds) {
        Snackbar.make(view, view.getContext().getString(id), milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorRed))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }

    public static void onShowWaringAlertEvent(View view, String alert, int milliseconds) {
        Snackbar.make(view, alert, milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorOrange))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }

    public static void onShowWaringAlertEvent(View view, int id, int milliseconds) {
        Snackbar.make(view, view.getContext().getString(id), milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorOrange))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }

    public static void onShowProcessingAlertEvent(View view, String alert, int milliseconds) {
        Snackbar.make(view, alert, milliseconds)
                .setBackgroundTint(view.getContext().getColor(R.color.colorBlue))
                .setActionTextColor(view.getContext().getColor(android.R.color.white)).show();
    }
    
}
