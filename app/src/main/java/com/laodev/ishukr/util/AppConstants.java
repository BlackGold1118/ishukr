package com.laodev.ishukr.util;

import com.laodev.ishukr.model.Media;
import com.laodev.ishukr.model.PaymentAccount;
import com.laodev.ishukr.model.User;

public class AppConstants {

    public static int BANNER_DELAY = 2000;

    public static String gToken = "";
    public static String gType = "";

    public static User gUser = new User();
    public static Media gSelMedia = new Media();
    public static int gPayAccountIndex = -1;

}
