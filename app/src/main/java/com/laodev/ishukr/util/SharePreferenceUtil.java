package com.laodev.ishukr.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.model.PaymentAccount;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SharePreferenceUtil {

    private static SharedPreferences sharedPreferences;

    public static void getInstance(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), MODE_PRIVATE);
    }

    public static void setLoginEmail(String email) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("email", email);
            editor.apply();
        }
    }

    public static String getLoginEmail() {
        return sharedPreferences.getString("email", null);
    }

    public static void setLoginPassword(String pass) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("password", pass);
            editor.apply();
        }
    }

    public static void setPaymentAccount(String info) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("payment", info);
            editor.apply();
        }
    }

    public static List<PaymentAccount> getPaymentAccounts() {
        List<PaymentAccount> paymentAccounts = new ArrayList<>();
        String paymentsStr = sharedPreferences.getString("payment", null);
        if (paymentsStr != null) {
            Type listType = new TypeToken<List<PaymentAccount>>(){}.getType();
            paymentAccounts.addAll(new Gson().fromJson(paymentsStr, listType));
        }
        return paymentAccounts;
    }

    public static String getLoginPassword() {
        return sharedPreferences.getString("password", null);
    }

    public static void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
