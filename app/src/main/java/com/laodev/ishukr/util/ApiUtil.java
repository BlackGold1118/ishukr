package com.laodev.ishukr.util;

import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ApiUtil {

    private static final String BASE_URL = "https://ishukr.com/";

    public static final String URL_GET_TOKEN =      BASE_URL + "index.php?route=feed/rest_api/gettoken";
    public static final String URL_REGISTER =       BASE_URL + "index.php?route=rest/register/register";
    public static final String URL_LOGIN =          BASE_URL + "index.php?route=rest/login/login";
    public static final String URL_GET_WISHLIST =   BASE_URL + "index.php?route=rest/wishlist/wishlist";
    public static final String URL_GET_CATEGORY =   BASE_URL + "index.php?route=feed/rest_api/categories";
    public static final String URL_GET_BANNER =     BASE_URL + "index.php?route=feed/rest_api/banners";
    public static final String URL_GET_SEARCH =     BASE_URL + "index.php?route=feed/rest_api/products";
    public static final String URL_GET_MEDIA =      BASE_URL + "index.php?route=extension/extension/blog/article/getList";
    public static final String URL_COUNTRIES =      BASE_URL + "index.php?route=feed/rest_api/countries";
    public static final String URL_CONTACT =        BASE_URL + "index.php?route=rest/contact/send";
    public static final String URL_PAYMENT_METHOD = BASE_URL + "index.php?route=rest/payment_address/paymentaddress";

    public static final String URL_CHANGE_PASS =    BASE_URL + "index.php?route=rest/account/password";
    public static final String URL_ADD_ADDRESS =    BASE_URL + "index.php?route=rest/account/address";
    public static final String URL_GET_CART =       BASE_URL + "index.php?route=rest/cart/cart";
    public static final String URL_SAVE_CHANGE =    BASE_URL + "index.php?route=rest/account/account";

    public enum APIMethod {
        GET, POST, PUT, POSTSTR
    }

    public static void onAPIConnectionResponse (String url
            , Map<String, String> params
            , Map<String, String> headers
            , APIMethod method
            , APIManagerCallback apiResponse)
    {
        if (method == APIMethod.POST) {
            OkHttpUtils.post().url(url)
                    .params(params)
                    .headers(headers)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(okhttp3.Call call, Exception e, int id) {
                            apiResponse.onEventInternetError(e);
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                apiResponse.onEventCallBack(obj);
                            } catch (JSONException e) {
                                apiResponse.onEventServerError(e);
                                e.printStackTrace();
                            }
                        }
                    });
        } else if (method == APIMethod.GET) {
            OkHttpUtils.get().url(url)
                    .params(params)
                    .headers(headers)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(okhttp3.Call call, Exception e, int id) {
                            apiResponse.onEventInternetError(e);
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                apiResponse.onEventCallBack(obj);
                            } catch (JSONException e) {
                                apiResponse.onEventServerError(e);
                                e.printStackTrace();
                            }
                        }
                    });
        } else if (method == APIMethod.PUT) {
            OkHttpUtils.put().url(url)
                    .requestBody(RequestBody.create(null, params.get("value")))
                    .headers(headers)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(okhttp3.Call call, Exception e, int id) {
                            apiResponse.onEventInternetError(e);
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                apiResponse.onEventCallBack(obj);
                            } catch (JSONException e) {
                                apiResponse.onEventServerError(e);
                                e.printStackTrace();
                            }
                        }
                    });
        } else if (method == APIMethod.POSTSTR) {
            String param = params.get("value");
            OkHttpUtils.postString().url(url)
                    .content(param)
                    .mediaType(MediaType.parse("application/json; charset=utf-8"))
                    .headers(headers)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(okhttp3.Call call, Exception e, int id) {
                            apiResponse.onEventInternetError(e);
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                apiResponse.onEventCallBack(obj);
                            } catch (JSONException e) {
                                apiResponse.onEventServerError(e);
                                e.printStackTrace();
                            }
                        }
                    });
        }
    }

    public interface APIManagerCallback {
        void onEventCallBack(JSONObject obj);
        void onEventInternetError(Exception e);
        void onEventServerError(Exception e);
    }

}
