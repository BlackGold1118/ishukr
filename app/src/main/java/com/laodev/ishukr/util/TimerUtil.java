package com.laodev.ishukr.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimerUtil {

    public static Date getDateByFormat (String formatStr, String date) {
        SimpleDateFormat format = new SimpleDateFormat(formatStr, Locale.getDefault());
        try {
            return format.parse(date);
        } catch (ParseException ignored) {
        }
        return new Date();
    }

}
