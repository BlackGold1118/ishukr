package com.laodev.ishukr.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.laodev.ishukr.R;
import com.laodev.ishukr.model.Category;
import com.squareup.picasso.Picasso;

public class AccountView extends LinearLayout {

    private boolean isClick = false;
    private boolean isRed = false;
    private String title = "";
    private int icon;


    public AccountView(Context context, Category category) {
        super(context);

        this.isClick = true;
        this.isRed = false;
        this.title = category.name;

        setOrientation(LinearLayout.HORIZONTAL);
        LayoutInflater.from(context).inflate(R.layout.item_account, this, true);

        initView(category);
    }

    private void initView(Category category) {
        ImageView img_icon = findViewById(R.id.img_icon);
        Picasso.get()
                .load(category.image)
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(img_icon);
        TextView lbl_title = findViewById(R.id.lbl_title);
        lbl_title.setText(title);
    }

    public AccountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.AccountView);
        this.isClick = arr.getBoolean(R.styleable.AccountView_isClick, false);
        this.isRed = arr.getBoolean(R.styleable.AccountView_isRed, false);
        this.title = arr.getString(R.styleable.AccountView_title);
        this.icon = arr.getResourceId(R.styleable.AccountView_icon, 0);
        arr.recycle();

        setOrientation(LinearLayout.HORIZONTAL);
        LayoutInflater.from(context).inflate(R.layout.item_account, this, true);

        initView();
    }

    private void initView() {
        ImageView img_icon = findViewById(R.id.img_icon);
        img_icon.setImageResource(icon);
        TextView lbl_title = findViewById(R.id.lbl_title);
        lbl_title.setText(title);
        ImageView img_arrow = findViewById(R.id.img_arrow);
        if (!isClick) {
            img_arrow.setVisibility(GONE);
        }
        if (isRed) {
            lbl_title.setTextColor(getContext().getColor(R.color.colorLogout));
        }
    }

}
