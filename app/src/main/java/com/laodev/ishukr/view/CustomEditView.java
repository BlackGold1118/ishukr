package com.laodev.ishukr.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.laodev.ishukr.R;

public class CustomEditView extends LinearLayout {

    private String hint = "", inputType = "";
    private EditText edt_content;


    public CustomEditView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.CustomEditView);
        this.hint = arr.getString(R.styleable.CustomEditView_hint);
        this.inputType = arr.getString(R.styleable.CustomEditView_inputType);
        if (inputType == null) {
            inputType = "Text";
        }
        arr.recycle();

        setOrientation(LinearLayout.HORIZONTAL);
        LayoutInflater.from(context).inflate(R.layout.item_custom_edit, this, true);

        initView();
    }

    private void initView() {
        edt_content = findViewById(R.id.edt_content);
        edt_content.setHint(hint);
        switch (inputType) {
            case "Number" :
                edt_content.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case "Email" :
                edt_content.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case "Phone" :
                edt_content.setInputType(InputType.TYPE_CLASS_PHONE);
                break;
            case "Pass" :
                edt_content.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            default:
                edt_content.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
        }
    }

    public String getText() {
        return edt_content.getText().toString();
    }

    public void setText(String str) {
        edt_content.setText(str);
    }

}
