package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.dialog.CountryDialog;
import com.laodev.ishukr.model.CountryModel;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddAddressActivity extends AppCompatActivity {

    private View content;
    private EditText txt_first_name, txt_last_name, txt_country, txt_address_01, txt_address_02, txt_town_city, txt_postalcode;
    private CheckBox chk_default;

    private List<CountryModel> countryModels = new ArrayList<>();
    private String countryId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        txt_first_name = findViewById(R.id.txt_first_name);
        txt_last_name = findViewById(R.id.txt_last_name);
        txt_country = findViewById(R.id.txt_country);
        txt_country.setInputType(InputType.TYPE_NULL);

        txt_address_01 = findViewById(R.id.txt_address_01);
        txt_address_02 = findViewById(R.id.txt_address_02);
        txt_town_city = findViewById(R.id.txt_town_city);
        txt_postalcode = findViewById(R.id.txt_postalcode);
        chk_default = findViewById(R.id.chk_default);

        initEvent();
        initData();
    }

    private void initEvent() {
        txt_country.setOnClickListener(v -> {
            CountryDialog countryDialog = new CountryDialog(AddAddressActivity.this, countryModels);
            countryDialog.setCountryDialogListener(countryModel -> {
                countryId = countryModel.country_id;
                txt_country.setText(countryModel.name);
            });
            countryDialog.show();
        });
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_COUNTRIES, new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                try {
                    countryModels.clear();
                    Type listType = new TypeToken<List<CountryModel>>(){}.getType();
                    countryModels.addAll(new Gson().fromJson(obj.getString("data"), listType));

                    countryId = "1";
                    txt_country.setText("Afghanistan");
                } catch (JSONException e) {
                    e.printStackTrace();
                    BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
                dialog.dismiss();
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }


    public void onClickAddAddressBtn(View view) {
        String first_name = txt_first_name.getText().toString();
        if (first_name.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_first_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String last_name = txt_last_name.getText().toString();
        if (last_name.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_last_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String country = txt_country.getText().toString();
        String address_01 = txt_address_01.getText().toString();
        if (address_01.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_address1_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String address_02 = txt_address_02.getText().toString();
        if (address_02.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_address2_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String town_city = txt_town_city.getText().toString();
        if (town_city.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_town_city, AppConstants.BANNER_DELAY);
            return;
        }
        String postal_code = txt_postalcode.getText().toString();
        if (postal_code.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_postal_code, AppConstants.BANNER_DELAY);
            return;
        }
        String defaultSet = chk_default.isChecked()? "1" : "0";

        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("firstname", first_name);
        params.put("lastname", last_name);
        params.put("city", town_city);
        params.put("address_1", address_01);
        params.put("address_2", address_02);
        params.put("country_id", countryId);
        params.put("postcode", postal_code);
        params.put("default", defaultSet);
        params.put("zone_id", "1256");
        params.put("company", ".");

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_ADD_ADDRESS, params, headers, ApiUtil.APIMethod.POST, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                finish();
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
