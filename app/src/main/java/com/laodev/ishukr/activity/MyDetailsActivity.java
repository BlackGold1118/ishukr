package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.StringUtil;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import java.util.HashMap;

public class MyDetailsActivity extends AppCompatActivity {

    private View content;
    private ImageView img_avatar;
    private TextView lbl_avatar;
    private EditText txt_first_name, txt_last_name, txt_email, txt_phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_detail);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        img_avatar = findViewById(R.id.img_avatar);
        lbl_avatar = findViewById(R.id.lbl_avatar);
        setAvatarView("", AppConstants.gUser.firstname + " " + AppConstants.gUser.lastname);

        txt_first_name = findViewById(R.id.txt_first_name);
        txt_first_name.setText(AppConstants.gUser.firstname);
        txt_last_name = findViewById(R.id.txt_last_name);
        txt_last_name.setText(AppConstants.gUser.lastname);
        txt_email = findViewById(R.id.txt_email);
        txt_email.setText(AppConstants.gUser.email);
        txt_phone = findViewById(R.id.txt_phone);
        txt_phone.setText(AppConstants.gUser.telephone);
    }

    private void setAvatarView(String url, String name) {
        if (url.isEmpty()) {
            img_avatar.setVisibility(View.GONE);
            lbl_avatar.setVisibility(View.VISIBLE);
            lbl_avatar.setText(StringUtil.getFirstUpperCharacter(name));
        } else {
            img_avatar.setVisibility(View.VISIBLE);
            lbl_avatar.setVisibility(View.GONE);
            Picasso.get()
                    .load(url)
                    .fit().centerCrop()
                    .placeholder(R.drawable.img_back_account)
                    .error(R.drawable.img_back_account)
                    .into(img_avatar);
        }
    }

    public void onClickSaveBtn(View view) {
        boolean isChanged = false;

        String firstName = txt_first_name.getText().toString();
        if (firstName.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_first_empty, AppConstants.BANNER_DELAY);
            return;
        }
        if (!firstName.equals(AppConstants.gUser.firstname)) {
            isChanged = true;
        }
        String lastName = txt_last_name.getText().toString();
        if (lastName.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_last_empty, AppConstants.BANNER_DELAY);
            return;
        }
        if (!lastName.equals(AppConstants.gUser.lastname)) {
            isChanged = true;
        }
        String email = txt_email.getText().toString();
        if (email.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_empty, AppConstants.BANNER_DELAY);
            return;
        }
        if (!email.contains("@") || !email.contains(".co")) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_formate, AppConstants.BANNER_DELAY);
            return;
        }
        if (!email.equals(AppConstants.gUser.email)) {
            isChanged = true;
        }
        String phone = txt_phone.getText().toString();
        if (phone.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_phone_number, AppConstants.BANNER_DELAY);
            return;
        }
        if (!phone.equals(AppConstants.gUser.telephone)) {
            isChanged = true;
        }
        if (!isChanged) {
            BannerUtil.onShowWaringAlertEvent(content, R.string.banner_no_change, AppConstants.BANNER_DELAY);
            return;
        }

        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);
        headers.put("Content-Type", "application/json");

        HashMap<String, String> params = new HashMap<>();
        params.put("value", "{\"firstname\":\"" + firstName + "\"" +
                ",\"lastname\":\"" + lastName + "\"" +
                ",\"email\":\"" + email + "\"" +
                ",\"telephone\":\"" + phone + "\"" +
                "}");

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_SAVE_CHANGE, params, headers, ApiUtil.APIMethod.PUT, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();

                AppConstants.gUser.firstname = firstName;
                AppConstants.gUser.lastname = lastName;
                AppConstants.gUser.email = email;
                AppConstants.gUser.telephone = phone;
                finish();
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    public void onClickSeletAvatarImg(View view) {
        CropImage.activity()
                .setMaxCropResultSize(2048, 2048)
                .setAutoZoomEnabled(false)
                .start(this);
    }

}
