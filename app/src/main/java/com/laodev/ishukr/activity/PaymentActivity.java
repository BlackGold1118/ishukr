package com.laodev.ishukr.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.laodev.ishukr.R;
import com.laodev.ishukr.adapter.PaymentAdapter;
import com.laodev.ishukr.model.PaymentAccount;
import com.laodev.ishukr.util.AppManager;
import com.laodev.ishukr.util.SharePreferenceUtil;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity extends AppCompatActivity {

    private View content;
    private LinearLayout llt_no_card;
    private ListView lst_payment;

    private List<PaymentAccount> paymentAccounts = new ArrayList<>();
    private PaymentAdapter paymentAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
            toolBar.setOnMenuItemClickListener(item -> {
                if (item.getItemId() == R.id.action_add) {
                    AppManager.showOtherActivity(PaymentActivity.this, AddPaymentActivity.class, 0);
                    return true;
                }
                return false;
            });
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        llt_no_card = findViewById(R.id.llt_no_card);
        lst_payment = findViewById(R.id.lst_payment);
        paymentAdapter = new PaymentAdapter(this, paymentAccounts);
        paymentAdapter.setPaymentAdapterListener(account -> {
            paymentAccounts.remove(account);
            SharePreferenceUtil.setPaymentAccount(new Gson().toJson(paymentAccounts));
            initData();
        });
        lst_payment.setAdapter(paymentAdapter);

        initData();
    }

    private void initData() {
        paymentAccounts.clear();
        paymentAccounts.addAll(SharePreferenceUtil.getPaymentAccounts());
        if (paymentAccounts.size() == 0) {
            llt_no_card.setVisibility(View.VISIBLE);
            lst_payment.setVisibility(View.GONE);
        } else {
            llt_no_card.setVisibility(View.GONE);
            lst_payment.setVisibility(View.VISIBLE);
            paymentAdapter.notifyDataSetChanged();
        }
    }

    public void onClickAddPaymentLlt(View view) {
        AppManager.showOtherActivity(PaymentActivity.this, AddPaymentActivity.class, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }
}
