package com.laodev.ishukr.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONObject;

import java.util.HashMap;

public class ContactUsActivity extends AppCompatActivity {

    private View content;
    private EditText txt_first_name, txt_last_name, txt_email, txt_message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        txt_first_name = findViewById(R.id.txt_first_name);
        txt_last_name = findViewById(R.id.txt_last_name);
        txt_email = findViewById(R.id.txt_email);
        txt_message = findViewById(R.id.txt_message);
    }


    public void onClickSubmitBtn(View view) {
        String first_name = txt_first_name.getText().toString();
        if (first_name.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_first_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String last_name = txt_last_name.getText().toString();
        String email = txt_email.getText().toString();
        if (email.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_empty, AppConstants.BANNER_DELAY);
            return;
        }
        if (!email.contains("@") || !email.contains(".")) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_formate, AppConstants.BANNER_DELAY);
            return;
        }
        String message = txt_message.getText().toString();
        if (message.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_message_empty, AppConstants.BANNER_DELAY);
            return;
        }
        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("name", first_name + " " + last_name);
        params.put("email", email);
        params.put("enquiry", message);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_CONTACT, params, headers, ApiUtil.APIMethod.POST, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                finish();
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    public void onClickWhatsappImg(View view) {
        String phoneNumber = "+98123456789";
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("https://api.whatsapp.com/send?phone=" + phoneNumber));
        startActivity(sendIntent);
    }

    public void onClickPhoneImg(View view) {
        String busPhone = "123456789";
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + busPhone));
        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_permission, AppConstants.BANNER_DELAY);
            return;
        }
        startActivity(intent);
    }

}
