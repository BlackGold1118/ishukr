package com.laodev.ishukr.activity;

import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.AppConstants;

public class MediaDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_detail);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        TextView lbl_head = findViewById(R.id.lbl_head);
        TextView lbl_desc = findViewById(R.id.lbl_desc);

        lbl_head.setText(AppConstants.gSelMedia.heading_title);
        lbl_desc.setText(Html.fromHtml(AppConstants.gSelMedia.description));
    }

}
