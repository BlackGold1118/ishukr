package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.view.CustomEditView;

import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private View content;
    private CustomEditView cev_first, cev_last, cev_email, cev_pass, cev_repass, cev_phone;
    private RadioGroup rdg_gender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        cev_first = findViewById(R.id.cev_first);
        cev_last = findViewById(R.id.cev_last);
        cev_email = findViewById(R.id.cev_email);
        cev_pass = findViewById(R.id.cev_pass);
        cev_phone = findViewById(R.id.cev_phone);
        cev_repass = findViewById(R.id.cev_repass);

        rdg_gender = findViewById(R.id.rdg_gender);

        initEvent();
    }

    private void initEvent() {
        Button btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(v -> onClickRegister());

        RelativeLayout rlt_facebook = findViewById(R.id.rlt_facebook);
        rlt_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void onClickRegister() {
        String firstName = cev_first.getText();
        if (firstName.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_first_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String lastName = cev_last.getText();
        if (lastName.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_last_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String email = cev_email.getText();
        if (email.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_empty, AppConstants.BANNER_DELAY);
            return;
        }
        if (!email.contains("@") || !email.contains(".co")) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_formate, AppConstants.BANNER_DELAY);
            return;
        }
        String phone = cev_phone.getText();
        if (phone.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_phone_number, AppConstants.BANNER_DELAY);
            return;
        }
        String pass = cev_pass.getText();
        if (pass.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_pass_formate, AppConstants.BANNER_DELAY);
            return;
        }
        String repass = cev_repass.getText();
        if (!pass.equals(repass)) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_pass_match, AppConstants.BANNER_DELAY);
            return;
        }

        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("firstname", firstName);
        params.put("lastname", lastName);
        params.put("email", email);
        params.put("password", pass);
        params.put("confirm", repass);
        params.put("telephone", phone);
        params.put("agree", "1");

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_REGISTER, params, headers, ApiUtil.APIMethod.POST, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                finish();
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
