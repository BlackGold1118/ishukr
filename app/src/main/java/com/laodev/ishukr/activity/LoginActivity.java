package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.laodev.ishukr.R;
import com.laodev.ishukr.model.User;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;
import com.laodev.ishukr.view.CustomEditView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    private View content;
    private CustomEditView cev_email, cev_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        cev_email = findViewById(R.id.cev_email);
        cev_pass = findViewById(R.id.cev_pass);

        Button btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(v -> onClickLoginBtn());
        RelativeLayout rlt_facebook = findViewById(R.id.rlt_facebook);
        rlt_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void onClickLoginBtn() {
        String email = cev_email.getText();
        if (email.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_empty, AppConstants.BANNER_DELAY);
            return;
        }
        if (!email.contains("@") || !email.contains(".co")) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_email_formate, AppConstants.BANNER_DELAY);
            return;
        }
        String pass = cev_pass.getText();
        if (pass.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_pass_formate, AppConstants.BANNER_DELAY);
            return;
        }
        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", pass);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_LOGIN, params, headers, ApiUtil.APIMethod.POST, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    SharePreferenceUtil.setLoginEmail(email);
                    SharePreferenceUtil.setLoginPassword(pass);

                    AppConstants.gUser = new Gson().fromJson(obj.getString("data"), User.class);
                    finish();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
