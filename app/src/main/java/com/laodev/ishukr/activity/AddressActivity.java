package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.AppManager;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;

import org.json.JSONObject;

import java.util.HashMap;

public class AddressActivity extends AppCompatActivity {

    private View content;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        ListView lst_adress = findViewById(R.id.lst_adress);
    }


    public void onClickAddAddressLlt(View view) {
        AppManager.showOtherActivity(this, AddAddressActivity.class, 0);
    }

}
