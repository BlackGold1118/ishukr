package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.model.Product;
import com.laodev.ishukr.model.User;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.view.CustomEditView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WishListActivity extends AppCompatActivity {

    private View content;
    private RecyclerView ryc_list;
    private LinearLayout llt_empty;

    private List<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        llt_empty = findViewById(R.id.llt_empty);

        ryc_list = findViewById(R.id.ryc_list);
        ryc_list.setLayoutManager(new LinearLayoutManager(this));

        initData();
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_WISHLIST, null, headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                products.clear();
                Type listType = new TypeToken<List<Product>>(){}.getType();
                try {
                    products.addAll(new Gson().fromJson(obj.getString("data"), listType));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (products.size() > 0) {
                    llt_empty.setVisibility(View.GONE);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
