package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.AppManager;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.PermissionUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class OnBoardActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 451;

    private View content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard);

        SharePreferenceUtil.getInstance(this);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        initData();
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        AppConstants.gToken = "Basic ZWNvbW1lcmNlYXBwX25ldzpaV052YlcxbGNtTmxZWFkwaENjR0p0WkdaaU1rWT0=";

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Authorization", AppConstants.gToken);

        HashMap<String, String> param = new HashMap<>();
        param.put("grant_type", "client_credentials");

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_TOKEN, param, headers, ApiUtil.APIMethod.POST, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    AppConstants.gToken = obj.getJSONObject("data").getString("access_token");
                    AppConstants.gType = obj.getJSONObject("data").getString("token_type");
                } catch (JSONException e) {
                    e.printStackTrace();
                    BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    public void onClickExploreBtn(View view) {
        if (PermissionUtil.hasPermissions(this)) {
            AppManager.showOtherActivity(this, MainActivity.class, 0);
            finish();
        } else {
            ActivityCompat.requestPermissions(this, PermissionUtil.permissions, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionUtil.permissionsGranted(grantResults)) {
            AppManager.showOtherActivity(this, MainActivity.class, 0);
        } else {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }

}
