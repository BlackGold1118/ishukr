package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.laodev.ishukr.R;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;

import org.json.JSONObject;

import java.util.HashMap;

public class ChangePassActivity extends AppCompatActivity {

    private View content;
    private EditText txt_pass, txt_repass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        txt_pass = findViewById(R.id.txt_pass);
        txt_repass = findViewById(R.id.txt_repass);
    }


    public void onClickSaveBtn(View view) {
        String pass = txt_pass.getText().toString();
        if (pass.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_pass_formate, AppConstants.BANNER_DELAY);
            return;
        }
        String orgPass = SharePreferenceUtil.getLoginPassword();
        if (orgPass.equals(pass)) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_pass_same, AppConstants.BANNER_DELAY);
            return;
        }
        String repass = txt_repass.getText().toString();
        if (!pass.equals(repass)) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_pass_match, AppConstants.BANNER_DELAY);
            return;
        }

        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);
        headers.put("Content-Type", "application/json");

        HashMap<String, String> params = new HashMap<>();
        params.put("value", "{\"password\":\"" + pass + "\"" +
                ",\"confirm\":\"" + repass + "\"" +
                "}");

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_CHANGE_PASS, params, headers, ApiUtil.APIMethod.PUT, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();

                SharePreferenceUtil.setLoginPassword(pass);
                finish();
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
