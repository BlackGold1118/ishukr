package com.laodev.ishukr.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.laodev.ishukr.R;
import com.laodev.ishukr.fragment.AccountFragment;
import com.laodev.ishukr.fragment.CartFragment;
import com.laodev.ishukr.fragment.HomeFragment;
import com.laodev.ishukr.fragment.MediaFragment;
import com.laodev.ishukr.fragment.SearchFragment;
import com.laodev.ishukr.fragment.VerifyFragment;
import com.laodev.ishukr.model.User;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public View content;
    private Toolbar toolBar;
    private BottomNavigationView bottomNavigation;

    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> bottomNavigation.setSelectedItemId(R.id.action_home));
        }
    }

    @Override
    public void setTitle(int titleId) {
        toolBar.setTitle(titleId);
    }

    public void setVisibleToolbar(boolean flag) {
        if (flag) {
            toolBar.setVisibility(View.VISIBLE);
        } else {
            toolBar.setVisibility(View.GONE);
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(this);

        initData();
    }

    private void initData() {
        String email = SharePreferenceUtil.getLoginEmail();
        String pass = SharePreferenceUtil.getLoginPassword();
        if (email != null || pass != null) {
            ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.connect_server));

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

            HashMap<String, String> params = new HashMap<>();
            params.put("email", email);
            params.put("password", pass);

            ApiUtil.onAPIConnectionResponse(ApiUtil.URL_LOGIN, params, headers, ApiUtil.APIMethod.POST, new ApiUtil.APIManagerCallback() {
                @Override
                public void onEventCallBack(JSONObject obj) {
                    dialog.dismiss();
                    try {
                        AppConstants.gUser = new Gson().fromJson(obj.getString("data"), User.class);
                        loadFragmentByIndex(0);
                    } catch (JSONException e) {
                        BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
                    }
                }

                @Override
                public void onEventInternetError(Exception e) {
                    dialog.dismiss();
                    BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
                }

                @Override
                public void onEventServerError(Exception e) {
                    dialog.dismiss();
                    BannerUtil.onShowErrorAlertEvent(content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            });
        } else {
            loadFragmentByIndex(0);
        }
    }

    private void loadFragmentByIndex(int index) {
        this.index = index;
        Fragment frg = null;
        switch (index) {
            case 0:
                setTitle(R.string.app_name);
                frg = new HomeFragment(this);
                break;
            case 1:
                setTitle(R.string.title_search);
                frg = new SearchFragment(this);
                break;
            case 2:
                setTitle(R.string.title_media);
                frg = new MediaFragment(this);
                break;
            case 3:
                setTitle(R.string.title_bag);
                frg = new CartFragment(this);
                break;
            case 4:
                setTitle(R.string.title_account);
                if (AppConstants.gUser.firstname.isEmpty()) {
                    frg = new VerifyFragment(this);
                } else {
                    frg = new AccountFragment(this);
                }
                break;
        }

        onLoadFragment(frg);
    }

    private void onLoadFragment(Fragment frg) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction frgTran = fm.beginTransaction();
        frgTran.replace(R.id.frg_body, frg);
        frgTran.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setVisibleToolbar(true);
        switch (item.getItemId()) {
            case R.id.action_home:
                loadFragmentByIndex(0);
                break;
            case R.id.action_search:
                loadFragmentByIndex(1);
                break;
            case R.id.action_media:
                loadFragmentByIndex(2);
                break;
            case R.id.action_cart:
                loadFragmentByIndex(3);
                break;
            case R.id.action_account:
                loadFragmentByIndex(4);
                break;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (index == 4)
            loadFragmentByIndex(index);
    }

}
