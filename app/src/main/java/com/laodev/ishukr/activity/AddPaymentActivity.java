package com.laodev.ishukr.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.laodev.ishukr.R;
import com.laodev.ishukr.model.PaymentAccount;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;
import com.laodev.ishukr.util.TimerUtil;
import com.laodev.ishukr.view.CreditCardEditText;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class AddPaymentActivity extends AppCompatActivity {

    private View content;
    private CreditCardEditText cce_card;
    private EditText txt_expire, txt_name;
    private TextView lbl_card_number;
    private CheckBox chk_default;

    private List<PaymentAccount> paymentAccounts = new ArrayList<>();
    private PaymentAccount selPayment = new PaymentAccount();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        setToolbar();
        initView();
    }

    private void setToolbar() {
        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            toolBar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        content = findViewById(R.id.content);

        lbl_card_number = findViewById(R.id.lbl_card_number);
        cce_card = findViewById(R.id.cce_card);
        cce_card.setCreditCardEditTextListener(str -> lbl_card_number.setText(str));
        txt_expire = findViewById(R.id.txt_expire);
        txt_expire.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {
                    String[] expired_dates = s.toString().split("/");
                    if (Integer.parseInt(expired_dates[0]) > 12) {
                        txt_expire.setError("Wrong Date");
                    } else {
                        txt_expire.setError(null);
                    }
                    if (s.length() == 5) {
                        Date expried_date = TimerUtil.getDateByFormat("MM/yy", s.toString());
                        Date current_date = new Date();
                        if (current_date.after(expried_date)) {
                            txt_expire.setError("This card was already expried.");
                        } else {
                            txt_expire.setError(null);
                        }
                    }
                } else {
                    String date = s.toString();
                    if (Integer.parseInt(date) > 12) {
                        txt_expire.setError("Wrong Date");
                    } else {
                        txt_expire.setError(null);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 3) {
                    if (s.toString().contains("/")) {
                        s.delete(2, 3);
                    } else {
                        s.insert(2, "/");
                    }
                }
            }
        });
        txt_name = findViewById(R.id.txt_name);
        chk_default = findViewById(R.id.chk_default);

        if (AppConstants.gPayAccountIndex > -1) {
            paymentAccounts.clear();
            paymentAccounts.addAll(SharePreferenceUtil.getPaymentAccounts());
            selPayment = paymentAccounts.get(AppConstants.gPayAccountIndex);
            initData();
        }
    }

    private void initData() {
        lbl_card_number.setText(selPayment.card_number);
        cce_card.setText(selPayment.card_number);
        txt_name.setText(selPayment.card_hold_name);
        txt_expire.setText(selPayment.expired);
        chk_default.setChecked(selPayment.isDefault);
    }

    public void onClickAddCardBtn(View view) {
        String name = txt_name.getText().toString();
        if (name.isEmpty()) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_name_empty, AppConstants.BANNER_DELAY);
            return;
        }
        String expire_date = txt_expire.getText().toString();
        if (txt_expire.getError() != null || expire_date.length() < 5) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_expire_date, AppConstants.BANNER_DELAY);
            return;
        }
        String card_number = Objects.requireNonNull(cce_card.getText()).toString().replace(" ", "");
        if (card_number.length() < 16) {
            BannerUtil.onShowErrorAlertEvent(content, R.string.banner_card_wrong, AppConstants.BANNER_DELAY);
            return;
        }

        PaymentAccount payAccount = new PaymentAccount();
        payAccount.card_hold_name = name;
        payAccount.card_number = card_number;
        payAccount.cvv = name;
        payAccount.expired = expire_date;
        payAccount.isDefault = chk_default.isChecked();

        if (AppConstants.gPayAccountIndex > -1) {
            paymentAccounts.set(AppConstants.gPayAccountIndex, payAccount);
        } else {
            if (payAccount.isDefault) {
                for (PaymentAccount paymentAccount: paymentAccounts) {
                    paymentAccount.isDefault = false;
                }
            }
            paymentAccounts.add(payAccount);
        }
        String payString = new Gson().toJson(paymentAccounts);
        SharePreferenceUtil.setPaymentAccount(payString);

        finish();
    }

}
