package com.laodev.ishukr.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.LoginActivity;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.activity.RegisterActivity;
import com.laodev.ishukr.util.AppManager;

public class VerifyFragment extends Fragment {

    private MainActivity mActivity;

    public VerifyFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verify, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        Button btn_login = view.findViewById(R.id.btn_login);
        btn_login.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, LoginActivity.class, 0));

        Button btn_register = view.findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppManager.showOtherActivity(mActivity, RegisterActivity.class, 0);
            }
        });

        TextView llt_cancel = view.findViewById(R.id.llt_cancel);
        llt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
