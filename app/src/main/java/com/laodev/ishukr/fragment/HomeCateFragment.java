package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.CateGridAdapter;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeCateFragment extends Fragment {

    private MainActivity mActivity;

    private List<Category> categories = new ArrayList<>();
    private CateGridAdapter cateGridAdapter;


    HomeCateFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cate, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        mActivity.setVisibleToolbar(true);
        mActivity.setTitle(R.string.app_name);

        ListView lst_cate = view.findViewById(R.id.lst_cate);
        cateGridAdapter = new CateGridAdapter(mActivity, categories);
        lst_cate.setAdapter(cateGridAdapter);

        initData();
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_CATEGORY, new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    Type listType = new TypeToken<List<Category>>(){}.getType();
                    categories.addAll(new Gson().fromJson(obj.getString("data"), listType));
                    cateGridAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }


}
