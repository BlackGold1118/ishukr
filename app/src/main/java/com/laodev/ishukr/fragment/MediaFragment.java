package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.MediaListAdapter;
import com.laodev.ishukr.model.Media;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MediaFragment extends Fragment {

    private MainActivity mActivity;

    private List<Media> mediaList = new ArrayList<>();
    private MediaListAdapter mediaListAdapter;


    public MediaFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        ListView lst_media = view.findViewById(R.id.lst_media);
        mediaListAdapter = new MediaListAdapter(mActivity, mediaList);
        lst_media.setAdapter(mediaListAdapter);

        initData();
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_MEDIA, new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    mediaList.clear();
                    Type listType = new TypeToken<List<Media>>(){}.getType();
                    mediaList.addAll(new Gson().fromJson(obj.getString("data"), listType));
                    mediaListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
