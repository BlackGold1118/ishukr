package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.ChildCateAdapter;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.model.Product;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductListFragment extends Fragment {

    private MainActivity mActivity;

    private Category category;
    private List<Product> products = new ArrayList<>();
    private ChildCateAdapter childCateAdapter;


    public ProductListFragment(MainActivity activity, Category category) {
        mActivity = activity;
        this.category = category;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        setToolbar(view);
        initView(view);
        return view;
    }

    private void setToolbar(View view) {
        mActivity.setVisibleToolbar(false);

        Toolbar toolBar = view.findViewById(R.id.toolbar);
        toolBar.setTitle(category.name);
        toolBar.setNavigationOnClickListener(v -> mActivity.onBackPressed());
        toolBar.setOnMenuItemClickListener(item -> {
            String sort = "", order = "";
            switch (item.getItemId()) {
                case R.id.action_name_asc:
                    sort = "name";
                    order = "asc";
                    break;
                case R.id.action_name_desc:
                    sort = "name";
                    order = "desc";
                    break;
                case R.id.action_price_asc:
                    sort = "price";
                    order = "asc";
                    break;
                case R.id.action_price_desc:
                    sort = "price";
                    order = "desc";
                    break;
                case R.id.action_rating_asc:
                    sort = "rating";
                    order = "asc";
                    break;
                case R.id.action_rating_desc:
                    sort = "rating";
                    order = "desc";
                    break;
                case R.id.action_model_asc:
                    sort = "model";
                    order = "asc";
                    break;
                case R.id.action_model_desc:
                    sort = "model";
                    order = "desc";
                    break;
            }
            sortData(sort, order);
            return true;
        });
    }

    private void initView(View view) {
        ListView lst_products = view.findViewById(R.id.lst_products);
        childCateAdapter = new ChildCateAdapter(mActivity, products);
        lst_products.setAdapter(childCateAdapter);

        iniData();
    }

    private void iniData() {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_SEARCH + "&category=" + category.category_id, new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    products.clear();
                    Type listType = new TypeToken<List<Product>>(){}.getType();
                    products.addAll(new Gson().fromJson(obj.getString("data"), listType));
                    childCateAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    private void sortData(String sort, String order) {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_SEARCH + "&category=" + category.category_id + "&sort=" + sort + "&order=" + order
                , new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    products.clear();
                    Type listType = new TypeToken<List<Product>>(){}.getType();
                    products.addAll(new Gson().fromJson(obj.getString("data"), listType));
                    childCateAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
