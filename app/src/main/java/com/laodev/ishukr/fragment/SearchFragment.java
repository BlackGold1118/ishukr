package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.CateGridAdapter;
import com.laodev.ishukr.adapter.ChildCateAdapter;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.model.Product;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchFragment extends Fragment {

    private MainActivity mActivity;

    private List<Product> products = new ArrayList<>();
    private ChildCateAdapter productGridAdapter;

    public SearchFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        ListView lst_cate = view.findViewById(R.id.lst_cate);
        productGridAdapter = new ChildCateAdapter(mActivity, products);
        lst_cate.setAdapter(productGridAdapter);

        SearchView searchView = view.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                initData(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void initData(String searchKey) {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("search", searchKey);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_SEARCH, params, headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    Type listType = new TypeToken<List<Product>>(){}.getType();
                    products.addAll(new Gson().fromJson(obj.getString("data"), listType));
                    productGridAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
