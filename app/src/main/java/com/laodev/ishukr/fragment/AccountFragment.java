package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.AddressActivity;
import com.laodev.ishukr.activity.ChangePassActivity;
import com.laodev.ishukr.activity.ContactUsActivity;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.activity.MyDetailsActivity;
import com.laodev.ishukr.activity.PaymentActivity;
import com.laodev.ishukr.activity.WishListActivity;
import com.laodev.ishukr.model.Media;
import com.laodev.ishukr.model.User;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.AppManager;
import com.laodev.ishukr.util.BannerUtil;
import com.laodev.ishukr.util.SharePreferenceUtil;
import com.laodev.ishukr.util.StringUtil;
import com.laodev.ishukr.view.AccountView;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;

public class AccountFragment extends Fragment {

    private MainActivity mActivity;

    private ImageView img_avatar, img_intro;
    private TextView lbl_avatar, lbl_name;
    private AccountView acv_basic, acv_wishlist, acv_logout, acv_pass, acv_address, acv_contact, acv_payment;

    public AccountFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        img_intro = view.findViewById(R.id.img_intro);
        img_avatar = view.findViewById(R.id.img_avatar);
        lbl_avatar = view.findViewById(R.id.lbl_avatar);
        lbl_name = view.findViewById(R.id.lbl_name);
        setAvatarView("", AppConstants.gUser.firstname + " " + AppConstants.gUser.lastname);

        acv_basic = view.findViewById(R.id.acv_basic);
        acv_wishlist = view.findViewById(R.id.acv_wishlist);
        acv_pass = view.findViewById(R.id.acv_pass);
        acv_address = view.findViewById(R.id.acv_address);
        acv_contact = view.findViewById(R.id.acv_contact);
        acv_payment = view.findViewById(R.id.acv_payment);
        acv_logout = view.findViewById(R.id.acv_logout);

        initEvent();
    }

    private void setAvatarView(String url, String name) {
        lbl_name.setText(name);
        if (url.isEmpty()) {
            img_avatar.setVisibility(View.GONE);
            lbl_avatar.setVisibility(View.VISIBLE);
            lbl_avatar.setText(StringUtil.getFirstUpperCharacter(name));
        } else {
            img_avatar.setVisibility(View.VISIBLE);
            lbl_avatar.setVisibility(View.GONE);
            Picasso.get()
                    .load(url)
                    .fit().centerCrop()
                    .placeholder(R.drawable.img_back_account)
                    .error(R.drawable.img_back_account)
                    .into(img_avatar);
            Picasso.get()
                    .load(url)
                    .fit().centerCrop()
                    .placeholder(R.drawable.img_back_account)
                    .error(R.drawable.img_back_account)
                    .into(img_intro);
        }
    }

    private void initEvent() {
        acv_basic.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, MyDetailsActivity.class, 0));
        acv_wishlist.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, WishListActivity.class, 0));
        acv_pass.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, ChangePassActivity.class, 0));
        acv_address.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, AddressActivity.class, 0));
        acv_contact.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, ContactUsActivity.class, 0));
        acv_payment.setOnClickListener(v -> AppManager.showOtherActivity(mActivity, PaymentActivity.class, 0));
        acv_logout.setOnClickListener(v -> {
            ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

            ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_CATEGORY, new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
                @Override
                public void onEventCallBack(JSONObject obj) {
                    dialog.dismiss();

                    SharePreferenceUtil.clear();
                    AppConstants.gUser = new User();
                    AppConstants.gSelMedia = new Media();
                    mActivity.onBackPressed();
                }

                @Override
                public void onEventInternetError(Exception e) {
                    dialog.dismiss();
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }

                @Override
                public void onEventServerError(Exception e) {
                    dialog.dismiss();
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            });
        });
    }

}
