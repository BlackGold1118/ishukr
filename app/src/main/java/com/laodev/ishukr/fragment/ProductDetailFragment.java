package com.laodev.ishukr.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.ADSliderAdapter;
import com.laodev.ishukr.model.Banner;
import com.laodev.ishukr.model.Product;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment implements View.OnClickListener {

    private MainActivity mainActivity;
    private Product mProduct;
    private ADSliderAdapter adSliderAdapter;
    private List<Product> mRelatedProducts = new ArrayList<>();
    private View view;

    private SliderView adSlider;
    private List<Banner> bannerList = new ArrayList<>();
    private List<String> typeList = new ArrayList<>();

    private boolean isShowDescription = false;
    private boolean isShowReturn = false;

    private TextView txtDescription, txtReturn, txtPrice, txtQTY, txtType;
    private Spinner spType;
    private ImageView imgAddQTY, imgReduceQTY, imgShowDesc, imgShowRet;
    private LinearLayout llt_type, llt_buy_more;


    public ProductDetailFragment(MainActivity activity, Product product) {
        // Required empty public constructor
        mainActivity = activity;
        mProduct = product;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        initData();
        initView(view);
        initBanner();
        return view;
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(mainActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("id", String.valueOf(mProduct.id));
        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_SEARCH, params, headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    JSONObject data = obj.getJSONObject("data");
                    JSONArray optionArr = data.getJSONArray("options");
                    if (optionArr.length() == 0) {
                        txtType.setVisibility(View.GONE);
                        spType.setVisibility(View.GONE);
                        llt_type.setVisibility(View.GONE);
                    } else {
                        txtType.setVisibility(View.VISIBLE);
                        spType.setVisibility(View.VISIBLE);
                        llt_type.setVisibility(View.VISIBLE);
                        typeList.clear();
                        JSONArray optionValueArr = optionArr.getJSONObject(0).getJSONArray("option_value");
                        for (int i = 0; i < optionValueArr.length(); i++) {
                            JSONObject option = optionValueArr.getJSONObject(i);
                            String optionName = option.getString("name");
                            typeList.add(optionName);
                        }
                        txtType.setText(typeList.get(0));
                    }

                    JSONArray cateArr = data.getJSONArray("category");
                    loadRealtedProduct(cateArr.getJSONObject(0).getInt("id"));

                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mainActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mainActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mainActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    private void loadRealtedProduct(int id) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
//        params.put("category", String.valueOf(id));
        params.put("category", "");
        params.put("limit", "20");
        params.put("page", "1");
        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_SEARCH, params, headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                try {
                    mRelatedProducts.clear();
//                    JSONObject data = obj.getJSONObject("data");
                    Type listType = new TypeToken<List<Product>>(){}.getType();
                    mRelatedProducts.addAll(new Gson().fromJson(obj.getString("data"), listType));
                    initRelatedProducts();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mainActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                BannerUtil.onShowErrorAlertEvent(mainActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                BannerUtil.onShowErrorAlertEvent(mainActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    private void initRelatedProducts() {
        llt_buy_more.removeAllViews();
        for (Product product: mRelatedProducts) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_related_product, null, false);
            ImageView img_cate = view.findViewById(R.id.img_cate);
            TextView txtName = view.findViewById(R.id.lbl_name);
            txtName.setText(product.name);
            TextView txtPrice = view.findViewById(R.id.lbl_price);
            txtPrice.setText(product.price_formated);
            Picasso.get()
                    .load(product.image)
                    .placeholder(R.drawable.ic_image_blue)
                    .error(R.drawable.ic_image_blue)
                    .into(img_cate);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProduct = product;
                    initData();
                    initView(view);
                    initBanner();
                }
            });
            llt_buy_more.addView(view);
        }
    }

    private void initBanner() {
        bannerList.clear();
        for (String image: mProduct.images) {
            Banner banner = new Banner();
            banner.image = image;
            banner.title = "";
            banner.link = "";
            bannerList.add(banner);
        }
        adSliderAdapter.notifyDataSetChanged();
    }

    private void initView(View view) {
        mainActivity.setVisibleToolbar(true);

        adSlider = view.findViewById(R.id.sld_home_ad);
        adSliderAdapter = new ADSliderAdapter(mainActivity, bannerList);
        adSlider.setSliderAdapter(adSliderAdapter);

        adSlider.setIndicatorAnimation(IndicatorAnimations.WORM);
        adSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        adSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        adSlider.setIndicatorSelectedColor(Color.WHITE);
        adSlider.setIndicatorUnselectedColor(Color.GRAY);
        adSlider.setScrollTimeInSec(4);
        adSlider.startAutoCycle();

        TextView txtTitle = view.findViewById(R.id.txt_title);
        txtTitle.setText(mProduct.name);

        LinearLayout lltSave = view.findViewById(R.id.llt_save);
        lltSave.setOnClickListener(this);

        LinearLayout lltShare = view.findViewById(R.id.llt_share);
        lltShare.setOnClickListener(this);

        txtPrice = view.findViewById(R.id.lbl_price);
        txtPrice.setText(mProduct.price_formated);

        txtDescription = view.findViewById(R.id.txt_description);

        txtReturn = view.findViewById(R.id.txt_return);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtReturn.setText(Html.fromHtml(mProduct.delivery_returns_info, Html.FROM_HTML_MODE_COMPACT));
            txtDescription.setText(Html.fromHtml(mProduct.description, Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtReturn.setText(Html.fromHtml(mProduct.delivery_returns_info));
            txtDescription.setText(Html.fromHtml(mProduct.description));
        }

        initShowDescriptionAndReturn();

        txtQTY = view.findViewById(R.id.txt_qty);
        txtQTY.setText("1");

        txtType = view.findViewById(R.id.txt_type);
        spType = view.findViewById(R.id.sp_type);

        imgAddQTY = view.findViewById(R.id.img_qty_plus);
        imgAddQTY.setOnClickListener(this);

        imgReduceQTY = view.findViewById(R.id.img_qty_min);
        imgReduceQTY.setOnClickListener(this);

        imgShowDesc = view.findViewById(R.id.img_add_desc);
        imgShowDesc.setOnClickListener(this);

        imgShowRet = view.findViewById(R.id.img_add_return);
        imgShowRet.setOnClickListener(this);

        llt_type = view.findViewById(R.id.llt_type);
        llt_type.setOnClickListener(this);

        llt_buy_more = view.findViewById(R.id.llt_buy_more);
    }

    private void initShowDescriptionAndReturn() {
        if (!isShowDescription) {
            txtDescription.setVisibility(View.GONE);
        } else {
            txtDescription.setVisibility(View.VISIBLE);
        }

        if (!isShowReturn) {
            txtReturn.setVisibility(View.GONE);
        } else {
            txtReturn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llt_save:

                break;
            case R.id.llt_share:

                break;
            case R.id.img_add_desc:
                isShowDescription = !isShowDescription;
                initShowDescriptionAndReturn();
                break;
            case R.id.img_add_return:
                isShowReturn = !isShowReturn;
                initShowDescriptionAndReturn();
            case R.id.img_qty_plus:
                 onControlQTY(true);
                break;
            case R.id.img_qty_min:
                onControlQTY(false);
                break;
            case R.id.llt_type:
                onShowPopMenu();
                break;
        }
    }

    private void onControlQTY(boolean b) {
        int qty = Integer.valueOf(txtQTY.getText().toString());
        if (b) {
            if (qty == mProduct.quantity) {
                BannerUtil.onShowErrorAlertEvent(mainActivity.content, "Only " + mProduct.quantity + " items in stock", AppConstants.BANNER_DELAY);
            } else {
                txtQTY.setText(String.valueOf(qty + 1));
            }
        } else {
            if (qty == 1) {
                return;
            } else {
                txtQTY.setText(String.valueOf(qty - 1));
            }
        }
    }

    private void onShowPopMenu() {
        PopupMenu pop = new PopupMenu(mainActivity, llt_type);
        for (int i = 0 ; i < typeList.size(); i++) {
            pop.getMenu().add(0, i + 1, 1, typeList.get(i));
        }

        pop.setOnMenuItemClickListener(item -> {
            txtType.setText(item.getTitle().toString());
            return true;
        });
        pop.show();
    }
}
