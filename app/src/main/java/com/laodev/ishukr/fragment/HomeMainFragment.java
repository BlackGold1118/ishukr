package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.ADSliderAdapter;
import com.laodev.ishukr.model.Banner;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.model.User;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeMainFragment extends Fragment {

    private MainActivity mActivity;
    private SliderView adSlider, featureSlider;
    private List<Banner> bannerList = new ArrayList<>();
    private ADSliderAdapter adSliderAdapter;

    HomeMainFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initData();
        initUIView(view);
        return view;
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        HashMap<String, String> params = new HashMap<>();
        params.put("id", "7");
        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_BANNER, params, headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    bannerList.clear();
                    String bannerArr = obj.getString("data");
                    Type listType = new TypeToken<List<Banner>>(){}.getType();
                    bannerList.addAll(new Gson().fromJson(bannerArr, listType));
                    adSliderAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

    private void initUIView(View view) {
        mActivity.setVisibleToolbar(true);

        adSlider = view.findViewById(R.id.sld_home_ad);
        adSliderAdapter = new ADSliderAdapter(mActivity, bannerList);
        adSlider.setSliderAdapter(adSliderAdapter);

        adSlider.setIndicatorAnimation(IndicatorAnimations.WORM);
        adSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        adSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        adSlider.setIndicatorSelectedColor(Color.WHITE);
        adSlider.setIndicatorUnselectedColor(Color.GRAY);
        adSlider.setScrollTimeInSec(4);
        adSlider.startAutoCycle();

        featureSlider = view.findViewById(R.id.sld_home_feature);
        featureSlider.setVisibility(View.GONE);
    }

}
