package com.laodev.ishukr.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;
import com.laodev.ishukr.adapter.SubCateAdapter;
import com.laodev.ishukr.model.Category;
import com.laodev.ishukr.util.ApiUtil;
import com.laodev.ishukr.util.AppConstants;
import com.laodev.ishukr.util.BannerUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SubCateFragment extends Fragment {

    private MainActivity mActivity;

    private List<Category> categories = new ArrayList<>();
    private SubCateAdapter subCateAdapter;
    private Category supCategory;


    public SubCateFragment(MainActivity activity, Category category) {
        mActivity = activity;
        supCategory = category;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_cate, container, false);

        setToolbar(view);
        initView(view);
        return view;
    }

    private void setToolbar(View view) {
        mActivity.setVisibleToolbar(false);

        Toolbar toolBar = view.findViewById(R.id.toolbar);
        toolBar.setTitle(supCategory.name);
        toolBar.setNavigationOnClickListener(v -> mActivity.onBackPressed());
    }

    private void initView(View view) {
        ListView lst_cate = view.findViewById(R.id.lst_cate);
        subCateAdapter = new SubCateAdapter(mActivity, categories);
        lst_cate.setAdapter(subCateAdapter);

        initData();
    }

    private void initData() {
        ProgressDialog dialog = ProgressDialog.show(mActivity, "", getString(R.string.connect_server));

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", AppConstants.gType + " " + AppConstants.gToken);

        ApiUtil.onAPIConnectionResponse(ApiUtil.URL_GET_CATEGORY + "&id=" + supCategory.category_id, new HashMap<>(), headers, ApiUtil.APIMethod.GET, new ApiUtil.APIManagerCallback() {
            @Override
            public void onEventCallBack(JSONObject obj) {
                dialog.dismiss();
                try {
                    categories.clear();
                    Type listType = new TypeToken<List<Category>>(){}.getType();
                    categories.addAll(new Gson().fromJson(obj.getJSONObject("data").getString("sub_categories"), listType));
                    subCateAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
                }
            }

            @Override
            public void onEventInternetError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }

            @Override
            public void onEventServerError(Exception e) {
                dialog.dismiss();
                BannerUtil.onShowErrorAlertEvent(mActivity.content, e.getMessage(), AppConstants.BANNER_DELAY);
            }
        });
    }

}
