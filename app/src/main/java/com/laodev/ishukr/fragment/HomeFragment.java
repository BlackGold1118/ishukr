package com.laodev.ishukr.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.laodev.ishukr.R;
import com.laodev.ishukr.activity.MainActivity;

public class HomeFragment extends Fragment {

    private MainActivity mActivity;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public HomeFragment(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        tabLayout = view.findViewById(R.id.tap_home);
        viewPager = view.findViewById(R.id.vpg_home);
        tabLayout.post(() -> tabLayout.setupWithViewPager(viewPager));
        viewPager.setAdapter(new HomeFragmentAdapter(getChildFragmentManager()));
    }

    class HomeFragmentAdapter extends FragmentPagerAdapter {

        private String[] str_titles = {getString(R.string.title_tap_home), getString(R.string.title_tap_cate)};

        HomeFragmentAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        public Fragment getItem(int position) {
            if (position == 0) {
                return new HomeMainFragment(mActivity);
            } else {
                return new HomeCateFragment(mActivity);
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return str_titles[position];
        }
    }


}
